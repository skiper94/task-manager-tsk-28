package ru.apolyakov.tm.command.domain;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.service.IProjectService;
import ru.apolyakov.tm.api.service.ITaskService;
import ru.apolyakov.tm.api.service.IUserService;
import ru.apolyakov.tm.command.AbstractCommand;
import ru.apolyakov.tm.dto.Domain;
import ru.apolyakov.tm.exception.other.ServiceLocatorNotFoundException;

import java.util.Optional;

import static ru.apolyakov.tm.util.TerminalUtil.printConfirmCommand;

public abstract class AbstractDomainCommand extends AbstractCommand {

    @NotNull
    protected static final String BIND_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    protected static final String JAXB_BIND_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected static final String APPLICATION_TYPE = "application/json";

    @NotNull
    protected static final String FILE_BINARY = "data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "data.base64";

    @NotNull
    protected static final String FILE_FASTERXML_JSON = "data_fasterxml.json";

    @NotNull
    protected static final String FILE_FASTERXML_XML = "data_fasterxml.xml";

    @NotNull
    protected static final String FILE_FASTERXML_YAML = "data_fasterxml.yaml";

    @NotNull
    protected static final String FILE_JAXB_JSON = "data_jaxb.json";

    @NotNull
    protected static final String FILE_JAXB_XML = "data_jaxb.xml";


    {
        setNeedAuthorization(true);
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    protected IProjectService getProjectService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return  serviceLocator.getProjectService();
    }

    @NotNull
    protected ITaskService getTaskService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getTaskService();
    }

    @NotNull
    protected IUserService getUserService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getUserService();
    }

    protected void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        getProjectService().clear();
        getProjectService().addAll(domain.getProjects());
        getTaskService().clear();
        getUserService().addAll(domain.getUsers());
    }

    @NotNull
    protected Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(getProjectService().findAll());
        domain.setTasks(getTaskService().findAll());
        domain.setUsers(getUserService().findAll());
        return domain;
    }


    protected void logoutOnLoad() {
        printConfirmCommand("Data load complete");
        getAuthService().logout();
    }

}
