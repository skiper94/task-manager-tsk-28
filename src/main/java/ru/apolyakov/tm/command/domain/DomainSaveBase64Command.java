package ru.apolyakov.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.dto.Domain;
import ru.apolyakov.tm.enumerated.Role;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.Base64;

public class DomainSaveBase64Command extends AbstractDomainCommand{

    @NotNull
    private final static String NAME = "data-save-base64";

    @NotNull
    private final static String DESCRIPTION = "Saving data to base64 file";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        final byte[] bytes = byteArrayOutputStream.toByteArray();
        final byte[] base64data = Base64.getEncoder().encode(bytes);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_BASE64);
        fileOutputStream.write(base64data);
        fileOutputStream.flush();
        fileOutputStream.close();
    }


}
