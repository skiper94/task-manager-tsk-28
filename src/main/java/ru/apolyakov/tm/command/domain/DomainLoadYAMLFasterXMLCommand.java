package ru.apolyakov.tm.command.domain;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.dto.Domain;
import ru.apolyakov.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DomainLoadYAMLFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "data-load-yaml-fasterxml";

    @NotNull
    private final static String DESCRIPTION = "Loading data from yaml file (fasterxml)";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_YAML)));
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
        @NotNull final Domain domain = yamlMapper.readValue(yaml, Domain.class);
        setDomain(domain);
        logoutOnLoad();
    }

}
