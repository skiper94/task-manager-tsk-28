package ru.apolyakov.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.dto.Domain;
import ru.apolyakov.tm.enumerated.Role;

import java.io.FileOutputStream;

public class DomainSaveJsonFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "data-save-json-fasterxml";

    @NotNull
    private final static String DESCRIPTION = "Saving data to json file (fasterxml)";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
