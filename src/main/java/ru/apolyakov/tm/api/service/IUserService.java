package ru.apolyakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.IService;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.model.User;

public interface IUserService extends IService<User> {

    boolean unlockById(@NotNull String id);

    boolean unlockByLogin(@NotNull String login);

    void add(@NotNull String login, @NotNull String password, @NotNull String email, @NotNull Role role,
             @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    @Nullable
    User findByLogin(@NotNull String login);

    boolean lockById(@NotNull String id);

    boolean lockByLogin(@NotNull String login);

    @Nullable
    User removeByLogin(@NotNull String login);

    void setPassword(@NotNull String login, @NotNull String password);

    void setRole(@NotNull String login, @NotNull Role role);

    @Nullable
    User updateById(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull String email,
                    @NotNull Role role, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    @Nullable
    User updateByLogin(@NotNull String login, @NotNull String password, @NotNull String email,
                       @NotNull Role role, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    void clear();

}
