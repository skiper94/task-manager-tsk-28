package ru.apolyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.model.AbstractModel;

import java.util.List;

public interface IRepository<E extends AbstractModel> {

    void add(@NotNull E entity);

    void addAll(@Nullable List<E> entities);

    void clear();

    @NotNull
    List<E> findAll();

    @Nullable
    E findOneById(@NotNull String id);

    int getSize();

    boolean isEmpty();

    void remove(@NotNull E entity);

    @Nullable
    E removeOneById(@NotNull String id);

}
