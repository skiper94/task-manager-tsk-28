package ru.apolyakov.tm.command.domain;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.dto.Domain;
import ru.apolyakov.tm.enumerated.Role;

import java.io.FileOutputStream;

public class DomainSaveYAMLFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "data-save-yaml-fasterxml";

    @NotNull
    private final static String DESCRIPTION = "Saving data to yaml file (fasterxml)";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
        @NotNull final String xml = yamlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_YAML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
